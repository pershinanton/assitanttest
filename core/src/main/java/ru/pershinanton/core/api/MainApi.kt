package ru.pershinanton.core.api

import retrofit2.http.GET
import ru.pershinanton.core.exception.UnexpectedResponseApiException
import ru.pershinanton.core.exception.UnknownApiException
import ru.pershinanton.core.models.ApiResponse
import ru.pershinanton.core.models.CoreResponseData

interface MainApi {

    @GET("test/item")
    @Throws(
        UnknownApiException::class,
        UnexpectedResponseApiException::class
    )
    suspend fun getData(): ApiResponse<CoreResponseData?>

}