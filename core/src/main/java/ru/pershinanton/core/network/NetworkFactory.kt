package ru.pershinanton.core.network

import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.pershinanton.presentation.BuildConfig
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import kotlin.reflect.KClass

object NetworkFactory {

    fun okHttpClient(
        interceptors: List<Interceptor>?,
        networkInterceptors: List<Interceptor>?,
        timeout: Long
    ) = OkHttpClient.Builder().apply {
        connectTimeout(timeout, TimeUnit.SECONDS)
        writeTimeout(timeout, TimeUnit.SECONDS)
        readTimeout(timeout, TimeUnit.SECONDS)
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        addNetworkInterceptor(logging)
        if (interceptors != null) for (interceptor in interceptors) addInterceptor(
            interceptor
        )
        if (networkInterceptors != null) for (networkInterceptor in networkInterceptors) addNetworkInterceptor(
            networkInterceptor
        )
        skipCertificateValidation(this)
    }.build()

    private fun skipCertificateValidation(okHttpClientBuilder: OkHttpClient.Builder) {
        okHttpClientBuilder.hostnameVerifier { _, _ -> true }

        val trustManager = object : X509TrustManager {
            override fun getAcceptedIssuers(): Array<X509Certificate?> {
                return arrayOfNulls(0)
            }

            override fun checkServerTrusted(
                chain: Array<X509Certificate>,
                authType: String
            ) {
            }

            override fun checkClientTrusted(
                chain: Array<X509Certificate>,
                authType: String
            ) {
            }
        }
        try {
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, arrayOf<TrustManager>(trustManager), null)
            val sslSocketFactory = sslContext.socketFactory
            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustManager)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) e.printStackTrace()
        }
    }

    fun retrofit(gson: Gson, client: OkHttpClient, baseUrl: String): Retrofit = Retrofit.Builder()
        .client(client)
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    fun <T : Any> apiClient(retrofit: Retrofit, apiClientClass: KClass<T>): T =
        retrofit.create(apiClientClass.java)
}