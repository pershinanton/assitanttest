package ru.pershinanton.core.di

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import ru.pershinanton.core.interceptors.CheckResponseInterceptor
import ru.pershinanton.core.api.MainApi
import ru.pershinanton.core.network.NetworkFactory
import javax.inject.Singleton


@Module
class CoreModule {

    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideOkhttp(
        gson: Gson
    ) = NetworkFactory.okHttpClient(
        interceptors = listOf(),
        networkInterceptors = listOf(CheckResponseInterceptor(gson)),
        timeout = 5
    )

    @Provides
    @Singleton
    fun provideRetrofit(
        gson: Gson,
        okHttpClient: OkHttpClient
    ) = NetworkFactory.retrofit(
        gson = gson,
        client = okHttpClient,
        baseUrl = "https://89.208.230.60/"
    )

    @Provides
    @Singleton
    fun provideMainApi(
        retrofit: Retrofit
    ): MainApi = NetworkFactory.apiClient(
        retrofit = retrofit,
        MainApi::class
    )


}