package ru.pershinanton.core.useCase.base

abstract class UseCase<Result>  {
    abstract suspend operator fun invoke(): Result
}