package ru.pershinanton.core.useCase.base

abstract class BaseUseCase<Result> : UseCase<Result>()

abstract class BaseUseCaseWithParameters<Parameters, Result> :
    BaseUseCase<Result>() {

    abstract suspend fun invoke(parameters: Parameters): Result

    override suspend fun invoke(): Result {
        throw IllegalArgumentException("You must use method invoke(parameters) with parameters")
    }
}