package ru.pershinanton.core.useCase

import ru.pershinanton.core.repository.MainRepository
import ru.pershinanton.core.useCase.base.BaseUseCase
import ru.pershinanton.models.models.response.ResponseData
import javax.inject.Inject

class GetDataUseCaseNew @Inject constructor(
    private val repository: MainRepository
) : BaseUseCase<ResponseData?>() {

    override suspend fun invoke(): ResponseData {
        return repository.getData()
    }
}