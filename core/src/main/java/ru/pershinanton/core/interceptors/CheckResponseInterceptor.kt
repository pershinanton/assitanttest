package ru.pershinanton.core.interceptors

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import ru.pershinanton.core.exception.UnexpectedResponseApiException
import ru.pershinanton.core.exception.UnknownApiException
import ru.pershinanton.core.models.ApiResponse
import ru.pershinanton.presentation.BuildConfig

internal class CheckResponseInterceptor(
    private val gson: Gson
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        return getResponse(request, chain)
    }

    private fun getResponse(request: Request, chain: Interceptor.Chain): Response {
        val response = chain.proceed(request)
        val body = response.peekBody(Long.MAX_VALUE).string()
        val baseResponse = try {
            val type = object : TypeToken<ApiResponse<Any>>() {}.type
            gson.fromJson<ApiResponse<Any>>(body, type)
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            throw UnexpectedResponseApiException(body)
        }
        if(response.code != 200 && response.code != 201) throw UnknownApiException()
        if(baseResponse.data == null) throw UnknownApiException()
        return response
    }

}