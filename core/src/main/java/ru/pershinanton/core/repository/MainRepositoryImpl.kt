package ru.pershinanton.core.repository


import ru.pershinanton.core.api.MainApi
import ru.pershinanton.core.exception.UnexpectedResponseApiException
import ru.pershinanton.core.exception.UnknownApiException
import ru.pershinanton.core.mappers.map
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val api: MainApi
) : MainRepository {

    @Throws(
        UnknownApiException::class,
        UnexpectedResponseApiException::class
    )
    override suspend fun getData() = api.getData().data!!.map()

}