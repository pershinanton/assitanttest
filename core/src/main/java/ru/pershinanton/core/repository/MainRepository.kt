package ru.pershinanton.core.repository

import ru.pershinanton.core.exception.UnexpectedResponseApiException
import ru.pershinanton.core.exception.UnknownApiException
import ru.pershinanton.models.models.response.ResponseData

interface MainRepository {

    @Throws(
        UnknownApiException::class,
        UnexpectedResponseApiException::class
    )
    suspend fun getData(): ResponseData

}