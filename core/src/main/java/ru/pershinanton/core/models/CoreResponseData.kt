package ru.pershinanton.core.models

data class CoreResponseData(
    val src: String,
    val single: String,
    val split_v: String,
    val split_h: String
)
