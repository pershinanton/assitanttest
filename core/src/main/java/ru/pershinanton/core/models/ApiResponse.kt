package ru.pershinanton.core.models

import com.google.gson.annotations.SerializedName

data class ApiResponse<T>(
    @SerializedName("a")
    val a: String,

    @SerializedName("task_id")
    val task: Int,

    @SerializedName("status")
    val status: Int,

    @SerializedName("results")
    val data: T?

)
