package ru.pershinanton.core.exception

class UnknownApiException(message: String? = null) : Exception(message)