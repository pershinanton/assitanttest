package ru.pershinanton.core.exception

class UnexpectedResponseApiException(message: String? = null) : Exception(message)