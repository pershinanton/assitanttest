package ru.pershinanton.core.mappers

import ru.pershinanton.core.models.CoreResponseData
import ru.pershinanton.models.models.response.ResponseData

fun CoreResponseData.map() = ResponseData(
    src = src,
    single = single,
    split_v = split_v,
    split_h = split_h
)

fun List<CoreResponseData>.map() = map { model ->
    model.map()
}