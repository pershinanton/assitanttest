package ru.pershinanton.assistanttest

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import ru.pershinanton.common.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavBar()
    }

    private fun setupNavBar() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        bottom_nav.apply {
            setupWithNavController(navController = findNavController(R.id.nav_host_fragment))
            setOnItemSelectedListener {
                when (it.itemId) {
                    R.id.main -> {
                        setNewGraph(navHostFragment, R.navigation.main_nav_graph)
                    }
                    else -> {
                        return@setOnItemSelectedListener false
                    }
                }
                return@setOnItemSelectedListener true
            }
        }
    }

    private fun setNewGraph(myNavHostFragment: NavHostFragment, newGraph: Int) {
        myNavHostFragment.navController.navInflater.apply {
            myNavHostFragment.navController.graph = this.inflate(newGraph)
        }
    }
}