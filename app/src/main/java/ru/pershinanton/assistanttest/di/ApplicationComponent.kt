package ru.pershinanton.assistanttest.di

import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import ru.pershinanton.assistanttest.App
import ru.pershinanton.core.di.CoreModule
import ru.pershinanton.di.UiModule
import javax.inject.Singleton


@Component(modules = [AndroidSupportInjectionModule::class, UiModule::class, CoreModule::class, ApplicationModule::class])
@Singleton
interface ApplicationComponent {

    fun inject(app: App)

}