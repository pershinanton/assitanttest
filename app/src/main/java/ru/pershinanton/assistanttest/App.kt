package ru.pershinanton.assistanttest

import android.app.Application
import androidx.fragment.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import ru.pershinanton.assistanttest.di.ApplicationComponent
import ru.pershinanton.assistanttest.di.ApplicationModule
import ru.pershinanton.assistanttest.di.DaggerApplicationComponent
import javax.inject.Inject

class App : Application(), HasSupportFragmentInjector {

    private lateinit var appComponent: ApplicationComponent

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        appComponent.inject(this)
    }

    override fun supportFragmentInjector() = fragmentInjector
}