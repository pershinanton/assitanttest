package ru.pershinanton.presentation.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2.ORIENTATION_HORIZONTAL
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main_screen.*
import ru.pershinanton.common.base.BaseFragment
import ru.pershinanton.presentation.R
import ru.pershinanton.presentation.adapter.ViewPagerAdapter
import ru.pershinanton.presentation.viewmodel.MainViewModel
import javax.inject.Inject

class MainScreenFragment : BaseFragment(layoutResId = R.layout.fragment_main_screen) {

    @Inject
    lateinit var viewModel: MainViewModel

    private val viewPagerAdapter: ViewPagerAdapter by lazy(LazyThreadSafetyMode.NONE) {
        ViewPagerAdapter(childFragmentManager, lifecycle).apply {
            addFragment(VideoFragment().newInstance(data[0]))
            addFragment(VideoFragment().newInstance(data[1]))
            addFragment(VideoFragment().newInstance(data[2]))
            addFragment(VideoFragment().newInstance(data[3]))
        }
    }
    private val data = arrayListOf<String>()
    private var toast: Toast? = null
    private val callback: OnPageChangeCallback = object : OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            if (viewPagerAdapter.fragments.size > position)
                viewPagerAdapter.fragments[position].onPageSelected()
            super.onPageSelected(position)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        viewModel.getData()
    }

    private fun setupViewPager(data: ArrayList<String>) {
        view_pager.apply {
            adapter = viewPagerAdapter
            orientation = ORIENTATION_HORIZONTAL
            offscreenPageLimit = 4
            registerOnPageChangeCallback(callback)
        }
    }

    private fun initObservers() {
        viewModel.successData.observe(viewLifecycleOwner, {
            it.apply {
                data.addAll(it)
                setupViewPager(data = it)
            }
        })
        viewModel.errorCode.observe(viewLifecycleOwner, {
            it.apply {
                showError(this)
            }
        })
    }

    private fun showError(error: String) {
        toast = Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).apply {
            show()
        }
    }

    override fun onStop() {
        super.onStop()
        view_pager?.unregisterOnPageChangeCallback(callback)
    }
}