package ru.pershinanton.presentation.view

import android.graphics.SurfaceTexture
import android.os.Bundle
import android.view.TextureView
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import kotlinx.android.synthetic.main.fragment_video.*
import kotlinx.android.synthetic.main.loading.*
import ru.pershinanton.common.base.BasePageFragment
import ru.pershinanton.common.base.OnPageSelectedListener
import ru.pershinanton.presentation.R
import ru.pershinanton.presentation.utils.SweetSurfaceTextureListener
import ru.pershinanton.presentation.utils.VideoController

class VideoFragment : BasePageFragment(layoutRes = R.layout.fragment_video), Player.EventListener,
    OnPageSelectedListener {

    companion object {
        const val URL_VALUE = "url_value"
    }

    fun newInstance(url: String) =
        VideoFragment().apply {
            arguments = bundleOf(Pair(URL_VALUE, url))
        }

    private val data get() = requireArguments().getString(URL_VALUE)
    private var isStart = false
    private var toast: Toast? = null

    private val videoController: VideoController by lazy(LazyThreadSafetyMode.NONE) {
        VideoController(
            textureView,
            requireContext().packageName,
            videoPlayerControllerListener
        )
    }

    private val videoPlayerControllerListener =
        object : VideoController.VideoPlayerControllerListener {
            override fun onError(
                textureView: TextureView,
                player: SimpleExoPlayer,
                error: ExoPlaybackException?
            ) {
                videoController.play(data!!, 0L)
                loading?.visibility = View.GONE
                showError(error?.cause?.message)
            }

            override fun onReady(textureView: TextureView, player: SimpleExoPlayer) {
                textureView.alpha = 1f
                loading?.visibility = View.GONE
                videoController.play()
            }

            override fun onStateChanged(
                textureView: TextureView,
                player: SimpleExoPlayer,
                state: VideoController.State?,
                isPlaying: Boolean
            ) {
            }

            override fun onProgress(
                textureView: TextureView,
                player: SimpleExoPlayer,
                currentPosition: Long
            ) {
            }

            override fun onComplete(textureView: TextureView, player: SimpleExoPlayer) {
                videoController.stop()
            }

            override fun onBuffering(textureView: TextureView, player: SimpleExoPlayer) {
                loading?.visibility = View.VISIBLE
            }

        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPlayer()
    }

    private fun setupPlayer() {
        if (textureView.isAvailable) {
            play(data!!, 0L)
        } else {
            textureView.surfaceTextureListener = object : SweetSurfaceTextureListener() {
                override fun onSurfaceTextureAvailable(
                    surfaceTexture: SurfaceTexture,
                    i: Int,
                    i1: Int
                ) {
                    textureView.surfaceTextureListener = null
                    play(data!!, 0L)
                }
            }
        }
    }

    private fun play(model: String, startPosition: Long) {
        loading?.visibility = View.VISIBLE
        isStart = true
        videoController.play(model, startPosition)
    }

    override fun onDestroyView() {
        videoController.stop()
        super.onDestroyView()
    }

    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {

    }

    override fun onTracksChanged(
        trackGroups: TrackGroupArray?,
        trackSelections: TrackSelectionArray?
    ) {

    }

    override fun onLoadingChanged(isLoading: Boolean) {
        loading?.visibility = View.GONE

    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

    }

    override fun onRepeatModeChanged(repeatMode: Int) {

    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

    }

    override fun onPlayerError(error: ExoPlaybackException?) {
        loading?.visibility = View.GONE
        showError(error?.message)
    }

    override fun onPositionDiscontinuity(reason: Int) {

    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

    }

    override fun onSeekProcessed() {

    }

    override fun onPageSelected() {
        if (textureView != null) {
            videoController.stop()
            setupPlayer()
        }
    }

    private fun showError(error: String?) {
        toast = Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).apply {
            show()
        }
    }

    override fun onPause() {
        toast?.cancel()
        super.onPause()
    }
}