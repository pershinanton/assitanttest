package ru.pershinanton.presentation.utils

import android.net.Uri
import android.os.Handler
import android.view.Surface
import android.view.TextureView
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.*
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.upstream.HttpDataSource.HttpDataSourceException.TYPE_CLOSE
import com.google.android.exoplayer2.util.Util
import java.io.IOException


class VideoController(
    private val textureView: TextureView,
    private val uuid: String,
    private val listener: VideoPlayerControllerListener
) : Player.EventListener {

    interface VideoPlayerControllerListener {
        fun onError(textureView: TextureView, player: SimpleExoPlayer, error: ExoPlaybackException?)
        fun onReady(textureView: TextureView, player: SimpleExoPlayer)
        fun onStateChanged(
            textureView: TextureView,
            player: SimpleExoPlayer,
            state: State?,
            isPlaying: Boolean
        )

        fun onProgress(textureView: TextureView, player: SimpleExoPlayer, currentPosition: Long)
        fun onComplete(textureView: TextureView, player: SimpleExoPlayer)
        fun onBuffering(textureView: TextureView, player: SimpleExoPlayer)

    }


    enum class State {
        PLAY, PAUSE, STOP
    }


    private val context get() = textureView.context
    private var player: SimpleExoPlayer? = null
    private val mainHandler = Handler()
    private val progressUpdateHandler = Handler()
    private val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
    private val videoTrackSelectionFactory: TrackSelection.Factory =
        AdaptiveTrackSelection.Factory(bandwidthMeter)
    private val trackSelector: TrackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
    private var renderersFactory = DefaultRenderersFactory(context)
    private var loadControl: LoadControl = DefaultLoadControl.Builder()
        .setAllocator(DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE))
        .setBufferDurationsMs(360000, 600000, 1000, 5000)
        .setTargetBufferBytes(C.LENGTH_UNSET)
        .createDefaultLoadControl()


    private val progressUpdateRunnable: Runnable by lazy {
        object : Runnable {
            override fun run() {
                if (player != null) {
                    listener.onProgress(textureView, player!!, player!!.currentPosition)
                    progressUpdateHandler.postDelayed(this, 100)
                }
            }
        }
    }

    private var state: State? = null

    private var isMuted = false
        private set
    private var currentState = 0
    private val isPlaying: Boolean
        get() {
            try {
                return state != null && state == State.PLAY
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) e.printStackTrace()
            }
            return false
        }

    private fun mute() {
        isMuted = true
        player?.volume = 0f
    }

    private fun unmute() {
        isMuted = false
        player?.volume = 1f
    }

    fun play(videoUrl: String, startPosition: Long) {
        if (player != null) return
        state = State.PLAY
        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl)
        listener.onStateChanged(textureView, player!!, state, isPlaying)
        if (isMuted) mute() else unmute()
        val surface = Surface(textureView.surfaceTexture)
        try {
            player?.removeListener(this)
            player?.addListener(this)
            player?.setVideoSurface(surface)
            val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(
                context,
                Util.getUserAgent(context, Util.getUserAgent(context, uuid))
            )
            if (videoUrl.contains(".m3u8")) {
                player?.prepare(
                    HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(videoUrl), mainHandler, null)
                )
            } else {
                player?.prepare(
                    ExtractorMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(videoUrl))
                )
            }
            if (startPosition > 0) player?.seekTo(startPosition)
        } catch (e: IllegalArgumentException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            stop()
        } catch (e: SecurityException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            stop()
        } catch (e: IllegalStateException) {
            if (BuildConfig.DEBUG) e.printStackTrace()
            stop()
        } catch (e: HttpDataSource.HttpDataSourceException){
            listener.onError(textureView,player!!,ExoPlaybackException.createForSource(IOException("Неработающий сервер")))
            if (BuildConfig.DEBUG) e.printStackTrace()
            stop()
        }
    }

    fun stop() {
        progressUpdateHandler.removeCallbacks(progressUpdateRunnable)
        if (player != null) {
            state = State.STOP
            listener.onStateChanged(textureView, player!!, state, isPlaying)
            player!!.removeListener(this)
            player!!.stop()
            player!!.release()
            player = null
        }
        textureView.surfaceTextureListener = null
    }

    fun play() {
        if (player != null) {
            state = State.PLAY
            listener.onStateChanged(textureView, player!!, state, isPlaying)
            player!!.playWhenReady = true
        }
    }


    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {

    }

    override fun onTracksChanged(
        trackGroups: TrackGroupArray?,
        trackSelections: TrackSelectionArray?
    ) {

    }

    override fun onLoadingChanged(isLoading: Boolean) {
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        if (currentState != playbackState) when (playbackState) {
            Player.STATE_ENDED -> if (true) player!!.seekTo(0) else {
//                        pause();
                listener.onComplete(textureView, player!!)
            }
            Player.STATE_READY -> {
                if (isPlaying) play()
                listener.onReady(textureView, player!!)
            }
            Player.STATE_BUFFERING -> {
                listener.onBuffering(textureView, player!!)
                player!!.playWhenReady = true
            }
        }
        currentState = playbackState
    }

    override fun onRepeatModeChanged(repeatMode: Int) {

    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

    }

    override fun onPlayerError(error: ExoPlaybackException?) {
        listener.onError(textureView, player!!, error)
        stop()
    }

    override fun onPositionDiscontinuity(reason: Int) {

    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

    }

    override fun onSeekProcessed() {

    }
}