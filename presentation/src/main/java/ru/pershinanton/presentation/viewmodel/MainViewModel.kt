package ru.pershinanton.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.pershinanton.common.base.BaseViewModel
import ru.pershinanton.core.useCase.GetDataUseCaseNew
import ru.pershinanton.models.models.response.ResponseData
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getDataUseCaseNew: GetDataUseCaseNew
) : BaseViewModel<ResponseData>() {

    val errorCode: MutableLiveData<String> = MutableLiveData()
    val successData: MutableLiveData<ArrayList<String>> = MutableLiveData()

    fun getData() {
        viewModelScope.launch {
            try {
                val data = getDataUseCaseNew.invoke()
                if (data != null) {
                    if (data.split_v == null && data.src == null) {
                        successData.value = arrayListOf(
                            "", data.single, "", data.split_h
                        )

                    } else {
                        successData.value = arrayListOf(
                            data.src, data.single, data.split_v, data.split_h
                        )
                    }
                } else {
                    errorCode.value = "Ошибка сервера"
                }
            } catch (t: Throwable) {
                errorCode.value = t.toString()
            }
        }
    }
}