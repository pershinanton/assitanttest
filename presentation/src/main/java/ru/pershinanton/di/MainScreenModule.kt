package ru.pershinanton.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.pershinanton.presentation.view.MainScreenFragment

@Module(
    includes = [
        ProvideViewModel::class
    ]
)
abstract class MainScreenModule {

    @ContributesAndroidInjector(
        modules = [
            InjectViewModel::class
        ]
    )
    abstract fun bind(): MainScreenFragment
}