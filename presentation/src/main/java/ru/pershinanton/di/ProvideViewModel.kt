package ru.pershinanton.di

import androidx.lifecycle.ViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ru.pershinanton.core.api.MainApi
import ru.pershinanton.core.repository.MainRepository
import ru.pershinanton.core.repository.MainRepositoryImpl
import ru.pershinanton.core.useCase.GetDataUseCaseNew
import ru.pershinanton.presentation.utils.ViewModelKey
import ru.pershinanton.presentation.viewmodel.MainViewModel

@Module
class ProvideViewModel {

    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun provideListNotesViewModel(getDataUseCaseNew: GetDataUseCaseNew): ViewModel =
        MainViewModel(getDataUseCaseNew)

    @Provides
    fun getUseCase(mainRepository: MainRepository): GetDataUseCaseNew =
        GetDataUseCaseNew(mainRepository)

    @Provides
    fun provideRepository(api: MainApi): MainRepository =
        MainRepositoryImpl(api = api)
}
