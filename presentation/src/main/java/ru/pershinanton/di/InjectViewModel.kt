package ru.pershinanton.di

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.pershinanton.presentation.view.MainScreenFragment
import ru.pershinanton.presentation.viewmodel.MainViewModel

@Module
class InjectViewModel {

    @Provides
    fun provideListNotesViewModel(
        factory: ViewModelProvider.Factory,
        target: MainScreenFragment
    ): MainViewModel =
        ViewModelProvider(target, factory).get(MainViewModel::class.java)
}