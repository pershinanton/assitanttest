package ru.pershinanton.common.base

interface OnPageSelectedListener {
    fun onPageSelected()
}