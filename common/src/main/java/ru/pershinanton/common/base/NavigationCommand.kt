package ru.pershinanton.common.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.core.os.bundleOf

sealed class NavigationCommand {
    data class ChooseBottomNavigation(val index: Int, val bundle: Bundle = bundleOf()): NavigationCommand()
    data class To(@IdRes val direction: Int, val bundle: Bundle = bundleOf()): NavigationCommand()
    data class BackTo(@IdRes val direction: Int): NavigationCommand()
    object Back: NavigationCommand()
}