package ru.pershinanton.common.base

interface OnHideNavBarInterface {
    fun hide()
    fun show()
}