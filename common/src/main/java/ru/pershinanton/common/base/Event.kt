package ru.pershinanton.common.base

open class Event<out T>(protected val content: T) {

    private var hasBeenHandled = false

    /**
     * Returns the content and prevents its use again.
     */
    open fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

class RepeatableEvent<out T>(content: T): Event<T>(content) {
    override fun getContentIfNotHandled(): T? = content
}