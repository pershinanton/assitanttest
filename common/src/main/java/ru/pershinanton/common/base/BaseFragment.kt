package ru.pershinanton.common.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment(
    @LayoutRes layoutResId: Int
) : Fragment(layoutResId), BackButtonListener {

    override fun onBackPressed() = true


}


