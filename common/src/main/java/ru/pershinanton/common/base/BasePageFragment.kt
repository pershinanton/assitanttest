package ru.pershinanton.common.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BasePageFragment(
    @LayoutRes layoutRes: Int
) : Fragment(layoutRes), OnPageSelectedListener

