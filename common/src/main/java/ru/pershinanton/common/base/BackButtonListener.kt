package ru.pershinanton.common.base

interface BackButtonListener {

    fun onBackPressed(): Boolean
}