package ru.pershinanton.common.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(),OnHideNavBarInterface {
    override fun hide() {

    }

    override fun show() {

    }
}

