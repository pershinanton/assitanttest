package ru.pershinanton.models.models.response

data class ResponseData(
    val src: String,
    val single: String,
    val split_v: String,
    val split_h: String
) {
    fun empty(): ResponseData {
        return ResponseData("", "", "", "")
    }
}
